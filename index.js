const getCube=5 ** 3;
console.log(`The cube of 5 is ${getCube}`)

const address=['258 Washington Ave NW','California','90111']
const [street,state,zip]=address
console.log(`I live at ${street}, ${state} ${zip}`)

const animal={
	givenName:'Charlie',
	animalType:'Cat',
	color:'Gray',
	breed:'Persian',
	height:'12 inches',
	weight:'10 pounds'
}
const {givenName,animalType,color,breed,height,weight}=animal
function getAnimal({givenName,animalType,color,breed,height,weight}){
	console.log(`${givenName} is a ${color} ${breed} ${animalType} which has a height of ${height} and weighs ${weight}`)
}
getAnimal(animal);

const numbers=[1,2,3,4,5];
numbers.forEach((n)=>console.log(n))

class Dog{
	constructor(name,age,breed){
		this.name=name;
		this.age=age;
		this.breed=breed;
	}
}

const myDog=new Dog('Loki','3','Husky')
console.log(myDog)